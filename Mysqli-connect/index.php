<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Connect to Mysqli</title>
</head>

<body>
    <h1>Connect db with mysqli</h1>
</body>

</html>

<?php
    $servername = "localhost";
    $username = "root";
    $password = "";

    $conn = mysqli_connect($servername, $username, $password);
    if (!$conn) {
        # code...
        die('Connection Failed! : ' . mysqli_connect_error());
    }
    echo "Connection Success";
?>